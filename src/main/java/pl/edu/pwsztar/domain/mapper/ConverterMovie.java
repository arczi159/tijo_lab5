package pl.edu.pwsztar.domain.mapper;

public interface ConverterMovie<T, F> {
    T convert(F from);
}
